variable "realm" {
    description = "Realm - Domain (Default dylanlab.xyz)"
    type = string
    default = "dylanlab.xyz"
}

variable "vm_count" {
    description = "Number of idential VMs required (Default 1)"
    type = number
    default = 1
}

variable "vm_name_prefix" {
    description = "VM hostname prefix (Default '')"
    type = string 
    default = ""
}

variable "vm_name_random_byte_length" {
    description = "VM hostname random byte length (default 8)"
    type = number
    default = 8
}

variable "vm_name_suffix" {
    description = "VM hostname suffix (Default '')"
    type = string 
    default = ""
}

variable "vm_name_description" {
    description = "VM Description"
    type = string
    default = "VM created with Terraform"
}

variable "vm_wait_for_ip" {
    description = "Wait for XOA IP"
    type = bool
    default = false
}

variable "vm_tags" {
    description = "Additional tags for VM"
    type = list(string)
    default = []
}

variable "xen_pool_name" {
    description = "XenOrchestra Pool"
    type = string
}

variable "xen_template_name" {
    description = "XenOrchestra Template"
    type = string
}

variable "xen_storage_name" {
    description = "XenOrchestra Storage"
    type = string
}

variable "xen_network_name" {
    description = "XenOrchestra Network"
    type = string
}

variable "vm_disk_size_gb" {
    description = "Disk size in Gb (Default 30)"
    default = 30
    type    = number
}

variable "vm_memory_size_gb" {
    description = "Memory in Gb (Default 4)"
    default = 4
    type    = number
}

variable "vm_cpu_count" {
    description = "CPU Count (Default 2)"
    default = 2
    type    = number
}

variable "username_ansible" {
    description = "Ansible account username"
    type = string
    default = "ansible"
}

variable "public_key_ansible" {
    type = string
    description = "Ansible account authorized key"
    default = ""
}

variable "username_admin" {
    description = "Administrator account username"
    type = string
    default = "admin"
}

variable "public_key_admin" {
    type = string
    description = "Administrator account authorized key"
    default = ""
}